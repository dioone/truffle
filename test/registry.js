var Registry = artifacts.require("./Registry.sol");

contract('Registry', function(accounts) {
	it("responds something to matrix() calls", function() {
		return Registry.deployed().then(function(instance) {
			return instance.adjacency_matrix.call();
		}).then(function(matrix) {
			console.log("matrix", matrix);
		});
	});
	it("knows its adjancy matrix list", function() {
		return Registry.deployed().then(function(instance) {
			return instance.getLength();
		}).then(function(length) {
			console.log("length", length, length.toString(10));
		});
	});
});
