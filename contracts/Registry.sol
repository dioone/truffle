pragma solidity 0.4.18;
//pragma experimental SMTChecker;
pragma experimental ABIEncoderV2;

contract Registry {
	bool[][] matrix;

	function Registry() {
		bool[] memory test = new bool[](1);
		test[0] = true;
		matrix.push(test);
	}
	function adjacency_matrix() returns (bool[][]) {
		return matrix;
	}
	function getLength() constant returns (uint length) {
		length = matrix.length;
	}
}
